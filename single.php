<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<h2><?php the_title(); ?></h2>
				<?php the_content(); ?>


				<?php 
$header = array('Content-Type: application/json');
$theID = get_post_meta(get_the_id(),'customid',true);
$url = "https://api.hubapi.com/contacts/v1/forms/".$theID."/?access_token=4d401811-5cec-11e3-89b3-1f820d9a8c43";


$ch = curl_init();
// Disable SSL verification
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// Will return the response, if false it print the response
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Set the url
curl_setopt($ch, CURLOPT_URL,$url);
// Execute
$result=curl_exec($ch);
$result = json_decode($result,true);
$fields = $result['fields'];
?>
<form method="POST" action="http://localhost:8888/Wordpress/wp-content/themes/twentythirteen/submitHubspot.php">
	<?php 
foreach ($fields as $field){ 
	if($field['fieldType']=="text" || $field['fieldType']=="file" || $field['fieldType']=="textarea" ) {
	?>
	<label for="<?php echo $field['name']; ?>"><?php echo $field['label']; ?></label>
	<input name="<?php echo $field['name']; ?>" type="<?php echo $field['fieldType']; ?>" > <br><br>
<?php 
}
	if($field['fieldType']=="checkbox") {
	?>
	<label for="<?php echo $field['name']; ?>"><?php echo $field['label']; ?></label><br>
	<?php 
	foreach ($field['options'] as $option) 
	{ ?>
	<input type="checkbox" name="vehicle" value="<?php echo $option['value']; ?>"> <?php echo $option['label']; ?> <br>
<?php 
}
}
} ?>
<input type="hidden" name="guid" value="<?php echo $result['guid']; ?>" >

<input type="submit" value="Go">
	</form>

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>